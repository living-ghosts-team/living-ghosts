﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MeleeEnemyController : EnemyController
{

    public float moveSpeed = 24f;
    public float jumpHeight = 8f;
    public float timeToJumpApex = 0.4f;
    public float accelerationTimeAir = 0.2f;
    public float accelerationTimeGround = 0.1f;
    public float maxViewDistance = 30f;
    public float attackInterval = 2f;

    public int attackDamage = 2;

    protected float gravity;
    protected float jumpVelocity;
    protected CharacterController2D characterController;
    protected Vector2 velocity;
    protected float velocityXSmoothing;
    protected Animator animator;
    protected GameObject collidedObject;

    protected bool canAttack = false;
    private float lastAttack;

    // Use this for initialization
    override protected void Start () {
        base.Start();

        characterController = GetComponent<CharacterController2D>();
        characterController.onTriggerEnterEvent += OnTriggerEnterEvent;

        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;

        animator = GetComponent<Animator>();
        lastAttack = Time.time;
    }

    void Update()
    {
        if (!collidedObject) canAttack = false;

        if (canAttack && ((Time.time - lastAttack) >= attackInterval))
        {
            lastAttack = Time.time;
            Attack();
        }

        Move();
    }
	
	void FixedUpdate() {
        velocity = characterController.velocity;

        if (characterController.isGrounded)
        {
            velocity.y = 0;
        }

        var players = FindObjectsOfType<PlayerController>();

        foreach (var player in players)
        {
            if (((PlayerController)player).isActiveCharacter)
            {
                float distanceX = player.transform.position.x - transform.position.x;

                if (Mathf.Abs(distanceX) < maxViewDistance && distanceX > 0f)
                {
                    GoRight();
                }
                else if (Mathf.Abs(distanceX) < maxViewDistance && distanceX < 0f)
                {
                    GoLeft();
                }
                else
                {
                    velocity.x = 0;
                }
            }
        }
    }

    virtual protected void Move()
    {
        velocity.y += gravity * Time.deltaTime;
        characterController.move(velocity * Time.deltaTime);
    }

    protected void GoRight()
    {
        velocity.x = Mathf.SmoothDamp(velocity.x, moveSpeed * 0.4f, ref velocityXSmoothing, characterController.isGrounded ? accelerationTimeGround : accelerationTimeAir);

        if (transform.localScale.x < 0f)
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    protected void GoLeft()
    {
        velocity.x = Mathf.SmoothDamp(velocity.x, moveSpeed * -0.4f, ref velocityXSmoothing, characterController.isGrounded ? accelerationTimeGround : accelerationTimeAir);

        if (transform.localScale.x > 0f)
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    virtual protected void onTriggerExitEvent(Collider2D obj)
    {
        if (collidedObject == obj.gameObject)
        {
            collidedObject = null;
            canAttack = false;
        }
    }

    void Attack()
    {
        animator.SetTrigger("Attack");
        collidedObject.GetComponent<PlayerController>().Hit(attackDamage);
    }

    void OnTriggerEnterEvent(Collider2D obj)
    {
        if (obj.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            collidedObject = obj.gameObject;

            if (obj.GetComponent<PlayerController>().isGhostCharacter)
            {
                SceneManager.LoadScene(2);
            } else
            {
                canAttack = true;
            }
        }
    }
}
