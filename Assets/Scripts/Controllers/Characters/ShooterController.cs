﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterController : PlayerController
{

    public float fireRate = 5;
    public GameObject bullet;
    public Transform muzzle;

    private float lastShot;

    // Use this for initialization
    override protected void Start () {
        base.Start();
        lastShot = Time.time;
    }
	
	override protected void Update() {
        base.Update();

        if (isActiveCharacter)
        {
            if (Input.GetButton("Fire1") && ((Time.time - lastShot) >= 1 / fireRate))
                Shoot();
        }
    }

    void Shoot()
    {
        animator.SetTrigger("Shoot");

        lastShot = Time.time;
        GameObject bulletInstance = Instantiate(bullet, muzzle.position, muzzle.rotation);
        bulletInstance.GetComponent<Renderer>().sortingLayerName = "Bullet";
        bulletInstance.GetComponent<BulletController>().character = gameObject;
    }
}
