﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeController : PlayerController
{

	// Use this for initialization
	override protected void Start () {
        base.Start();
    }
	
	override protected void Update() {
        base.Update();

        if (isActiveCharacter)
        {
            if (Input.GetButton("Fire1"))
                Attack();
        }
	}

    void Attack()
    {
        animator.SetTrigger("Attack");

        if (collidedObject && collidedObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            Destroy(collidedObject, 0.1f);
            collidedObject = null;
        }
    }
}
