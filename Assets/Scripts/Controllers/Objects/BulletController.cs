﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public float bulletSpeed = 50f;
    public int bulletDamage = 1;
    public GameObject character;

	// Use this for initialization
	void Start () {

        PlayerController[] controllers = FindObjectsOfType<PlayerController>();

        if (character.transform.localScale.x < 0)
        {
            bulletSpeed = -bulletSpeed;
            GetComponent<SpriteRenderer>().flipX = true;
        }
		
	}
	
	void FixedUpdate () {
        GetComponent<Rigidbody2D>().velocity = new Vector2(bulletSpeed, GetComponent<Rigidbody2D>().velocity.y);
        Destroy(gameObject, 3f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            Destroy(gameObject);
        }
        if (col.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            Destroy(gameObject);
        }
        else if (col.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            if (character.layer == LayerMask.NameToLayer("Player"))
            {
                col.gameObject.GetComponent<EnemyController>().Hit(bulletDamage);
                Destroy(gameObject);
            }
        }

        if (col.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (character.layer == LayerMask.NameToLayer("Enemy"))
            {
                col.gameObject.GetComponent<PlayerController>().Hit(bulletDamage);
                Destroy(gameObject);
            }
        }
    }
}
