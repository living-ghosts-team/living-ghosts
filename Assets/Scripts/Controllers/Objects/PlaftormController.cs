﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaftormController : MonoBehaviour {

    public float velocityY = 0.0f;
    public float velocityX = 0.1f;
    public float directionY = 1f;
    public float directionX = -1f;
    public float maxDistanceX = 5f;
    public float maxDistanceY = 0f;

    private Vector3 initialPosition;

	// Use this for initialization
	void Start () {
        initialPosition = transform.position;
	}
	
	void FixedUpdate () {
        var distance = transform.position - initialPosition;

        if (Mathf.Abs(distance.x) >= maxDistanceX)
            directionX = -directionX;

        if (Mathf.Abs(distance.y) >= maxDistanceY)
            directionY = -directionY;

        transform.position = new Vector2(directionX > 0f ? transform.position.x + velocityX : transform.position.x - velocityX,
        directionY > 0f ? transform.position.y + velocityY : transform.position.y - velocityY);
    }


    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            col.gameObject.transform.parent = transform;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Player"))
            col.gameObject.transform.parent = null;
    }

    void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.gameObject.layer == LayerMask.NameToLayer("Wall"))
            directionX = -directionX;

        if (obj.gameObject.layer == LayerMask.NameToLayer("Ground"))
            directionY = -directionY;
    }
}
